let passwordInput = document.querySelectorAll('input[type="password"]');
let passwordIcons = document.querySelectorAll('.icon-password');

passwordIcons.forEach(function(icon, index) {
  icon.addEventListener('click', function() {
    let isPasswordVisible = passwordInput[index].getAttribute('type') === 'text';
    passwordInput[index].setAttribute('type', isPasswordVisible ? 'password' : 'text');
    let isVisibleIcon = isPasswordVisible ? 'fa-eye-slash' : 'fa-eye';
    let isHiddenIcon = isPasswordVisible ? 'fa-eye' : 'fa-eye-slash';
    icon.classList.remove(isHiddenIcon);
    icon.classList.add(isVisibleIcon);
  });
});

let submitButton = document.querySelector('.btn');
submitButton.addEventListener('click', function(event) {
  event.preventDefault();

  let firstPassword = passwordInput[0].value;
  let secondPassword = passwordInput[1].value;

  if (firstPassword === secondPassword) {
    alert('You are welcome');
  } else {
    let errorMessage = document.createElement('span');
    errorMessage.textContent = 'Потрібно ввести однакові значення';
    errorMessage.style.color = 'red';
    passwordInput[1].parentNode.appendChild(errorMessage);
  }
});